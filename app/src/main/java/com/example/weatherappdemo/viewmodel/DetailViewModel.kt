package com.example.weatherappdemo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherappdemo.model.ListItem
import com.example.weatherappdemo.model.WeatherHourResponse
import java.text.SimpleDateFormat
import java.util.Locale

class DetailViewModel() : ViewModel() {
    var listDataToday: MutableLiveData<MutableList<ListItem>> = MutableLiveData(mutableListOf())
    var listDataFeature: MutableLiveData<MutableList<ListItem>> = MutableLiveData(mutableListOf())
    var listDataWeatherHourResponse: MutableLiveData<List<ListItem>> = MutableLiveData(listOf())
    var weatherHourResponse: WeatherHourResponse = WeatherHourResponse()
    private var dateStringPre = ""
    private var dateToday = ""

    fun cutWeatherHourToDataToday() {
        val listFeature = ArrayList<ListItem>()
        val listToday = ArrayList<ListItem>()
        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        dateToday = sdf.format(listDataWeatherHourResponse.value?.get(0)?.dt?.times(1000) ?: 0)
        listDataWeatherHourResponse.value?.forEach { listItem ->
            val date = sdf.format(listItem.dt?.times(1000) ?: 0)
            if (dateToday == date) {
                listToday.add(listItem)
            } else {
                if (dateStringPre != date) {
                    dateStringPre = date
                    listFeature.add(listItem)
                }
            }
        }
        listDataFeature.postValue(listFeature)
        listDataToday.postValue(listToday)

    }

    fun setListDataWeatherHourResponse(data: WeatherHourResponse) {
        this.weatherHourResponse = data
        listDataWeatherHourResponse.postValue(weatherHourResponse.list)
    }


}