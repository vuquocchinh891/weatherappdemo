package com.example.weatherappdemo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weatherappdemo.model.ListItem
import com.example.weatherappdemo.model.WeatherHourResponse
import com.example.weatherappdemo.model.WeatherResponse
import com.example.weatherappdemo.util.DataOnlineHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentMainViewModel : ViewModel() {
    var dataMain: MutableLiveData<WeatherResponse> = MutableLiveData(WeatherResponse())
    var dataForecast: MutableLiveData<List<ListItem>> = MutableLiveData(listOf())
    lateinit var weatherHourResponse: WeatherHourResponse

    fun getDataByLatLong(lat: Double, long: Double) {
        viewModelScope.launch(Dispatchers.IO) {
            DataOnlineHelper.apiService.getDataLocation(
                lat.toString(), long.toString()
            ).enqueue(object : Callback<WeatherResponse> {
                override fun onResponse(
                    call: Call<WeatherResponse>, response: Response<WeatherResponse>
                ) {
                    response.body()?.let {
                        dataMain.postValue(it)
                    }
                }

                override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                }

            })

            DataOnlineHelper.apiService.getDataForecastLocation(
                lat.toString(), long.toString()
            ).enqueue(object : Callback<WeatherHourResponse> {
                override fun onResponse(
                    call: Call<WeatherHourResponse>, response: Response<WeatherHourResponse>
                ) {
                    response.body()?.let {
                        weatherHourResponse = it
                        dataForecast.postValue(weatherHourResponse.list)
                    }
                }

                override fun onFailure(call: Call<WeatherHourResponse>, t: Throwable) {

                }

            })
        }

    }
}