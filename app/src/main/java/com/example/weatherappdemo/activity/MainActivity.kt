package com.example.weatherappdemo.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.weatherappdemo.R
import com.example.weatherappdemo.databinding.ActivityMainBinding
import com.example.weatherappdemo.dialog.ExitDialog
import com.example.weatherappdemo.fragment.MainFragment
import com.example.weatherappdemo.fragment.MapFragment
import com.example.weatherappdemo.util.Constants
import com.example.weatherappdemo.util.Constants.TAG_MAIN
import com.example.weatherappdemo.util.Constants.TAG_MAP

class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE_LOCATION = 6868
        const val CODE_SUSS = 200
    }

    private lateinit var fragmentManager: FragmentManager
    private lateinit var mainBinding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        fragmentManager = supportFragmentManager
        setContentView(mainBinding.root)
        checkPermission()
        onClick()
        replaceFragment(MainFragment(), TAG_MAIN)
    }

    private fun replaceFragment(newFragment: Fragment, tag: String) {
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.content_main, newFragment, tag)
        fragmentTransaction.addToBackStack(tag)
        fragmentTransaction.commit()
    }


    private fun onClick() {
        mainBinding.imageMainMap.setOnClickListener {
            checkPermission()
            if (ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                replaceFragment(MapFragment(), TAG_MAP)
            }
        }

        mainBinding.layoutMainButtonDetail.setOnClickListener {
            gotoDetail()
        }


        mainBinding.imageMainHome.setOnClickListener {
            replaceFragment(MainFragment(), TAG_MAIN)
        }


    }

    private fun gotoDetail() {
        val intentSearch = Intent(this, DetailActivity::class.java)
        when (val topFragment = getTopFragment()) {
            is MainFragment -> {
                intentSearch.putExtra(
                    Constants.KEY_DETAIL, topFragment.mFragmentMainViewModel.weatherHourResponse
                )
                startActivity(intentSearch)
            }

            is MapFragment -> {
                intentSearch.putExtra(
                    Constants.KEY_DETAIL, topFragment.fragmentMapViewModel.weatherHourResponse
                )
                startActivity(intentSearch)
            }
        }

    }


    private fun getTopFragment(): Fragment? {
        val backStackEntryCount = fragmentManager.backStackEntryCount
        if (backStackEntryCount > 0) {
            val topFragment = fragmentManager.getBackStackEntryAt(backStackEntryCount - 1)
            return fragmentManager.findFragmentByTag(topFragment.name)
        } else {
            return null
        }
    }

    private fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
        } else {
            try {
                val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            } catch (e: SecurityException) {

            }

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                replaceFragment(MainFragment(), TAG_MAIN)
            } else {
                checkPermission()
            }
        }
    }

    override fun onBackPressed() {
        ExitDialog(this, onExit = {
            finish()
        }).show()
    }
}


