package com.example.weatherappdemo.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.weatherappdemo.R
import com.example.weatherappdemo.adapter.ItemWeatherDailyAdapter
import com.example.weatherappdemo.adapter.ItemWeatherHourAdapter
import com.example.weatherappdemo.databinding.ActivityDetailBinding
import com.example.weatherappdemo.model.TypeAdapterHour
import com.example.weatherappdemo.model.WeatherHourResponse
import com.example.weatherappdemo.util.Constants
import com.example.weatherappdemo.viewmodel.DetailViewModel

class DetailActivity : AppCompatActivity() {
    private val mDetailViewModel by lazy {
        ViewModelProvider(this)[DetailViewModel::class.java]
    }
    private lateinit var mBinding: ActivityDetailBinding

    private val itemWeatherDailyAdapter = ItemWeatherDailyAdapter()

    private val itemWeatherHourAdapter =
        ItemWeatherHourAdapter(typeAdapterHour = TypeAdapterHour.DETAIL)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        intent?.let { intent ->
            if (intent.hasExtra(Constants.KEY_DETAIL)) {
                val detail = intent.getParcelableExtra<WeatherHourResponse>(Constants.KEY_DETAIL)
                detail?.let { dt ->
                    mDetailViewModel.setListDataWeatherHourResponse(dt)
                    mBinding.tvDetailNameCity.text =
                        dt.city?.name ?: getString(R.string.txt_city_default)
                }
            }
        }
        observer()
        setUpAdapter()
        onClick()
    }

    private fun onClick() {
        mBinding.imvDetailBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setUpAdapter() {
        mBinding.rcvDetailWeatherNextDay.apply {
            adapter = itemWeatherDailyAdapter
        }
        mBinding.rcvDetailWeatherHour.apply {
            adapter = itemWeatherHourAdapter
        }
    }

    private fun observer() {
        mDetailViewModel.listDataWeatherHourResponse.observe(this) { data ->
            if (data.isNotEmpty()) {
                mDetailViewModel.cutWeatherHourToDataToday()
            }
        }

        mDetailViewModel.listDataFeature.observe(this) { data ->
            if (data.isNotEmpty()) {
                itemWeatherDailyAdapter.setData(data)
            }

        }

        mDetailViewModel.listDataToday.observe(this) { data ->
            itemWeatherHourAdapter.setData(data)
        }
    }

}