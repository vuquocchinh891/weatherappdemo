package com.example.weatherappdemo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weatherappdemo.R
import com.example.weatherappdemo.base.GlobalApp
import com.example.weatherappdemo.databinding.ItemWeatherDailyBinding
import com.example.weatherappdemo.model.ListItem
import com.example.weatherappdemo.util.Util
import com.example.weatherappdemo.util.WeatherDiffUtil

class ItemWeatherDailyAdapter() :
    RecyclerView.Adapter<ItemWeatherDailyAdapter.ItemWeatherDailyHolder>() {

    private var listData: MutableList<ListItem> = mutableListOf()

    fun setData(newData: MutableList<ListItem>) {
        val diffUtil = DiffUtil.calculateDiff(WeatherDiffUtil(listData, newData))
        listData.clear()
        listData.addAll(newData)
        diffUtil.dispatchUpdatesTo(this)
    }

    inner class ItemWeatherDailyHolder(var itemWeatherDailyBinding: ItemWeatherDailyBinding) :
        RecyclerView.ViewHolder(itemWeatherDailyBinding.root) {
        fun setUpData(day: String, date: String, temp: String, urlImage: String) {
            itemWeatherDailyBinding.apply {
                tvItemDailyDay.text = date
                tvItemDailyDate.text = day
                Glide.with(imageItemDailyStatus).load(urlImage).into(imageItemDailyStatus)
                tvItemDailyTemp.text = itemView.context.getString(R.string.temperature, temp)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemWeatherDailyHolder {
        val itemWeatherDailyBinding =
            ItemWeatherDailyBinding.inflate(LayoutInflater.from(parent.context))
        return ItemWeatherDailyHolder(itemWeatherDailyBinding)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ItemWeatherDailyHolder, position: Int) {
        val dataDaily = listData[position]
        val date = if (position == 0) {
            GlobalApp.mContext.getString(R.string.txt_tomorrow)
        } else {
            Util.convertTime(dataDaily.dt ?: 0, "EEEE")
        }
        val day = Util.convertTime(dataDaily.dt ?: 0, "dd MMM")
        val urlImage = GlobalApp.mContext.getString(
            R.string.icon_url, dataDaily.weather?.get(0)?.icon ?: ""
        )
        val temp = (dataDaily.main?.temp?.times(0.1))?.toFloat()?.let { Util.formatTemperature(it) }
        holder.setUpData(day, date, temp.toString(), urlImage)
    }
}