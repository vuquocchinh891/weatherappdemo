package com.example.weatherappdemo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weatherappdemo.R
import com.example.weatherappdemo.databinding.ItemWeatherHousBinding
import com.example.weatherappdemo.model.ListItem
import com.example.weatherappdemo.model.Main
import com.example.weatherappdemo.model.TypeAdapterHour
import com.example.weatherappdemo.model.WeatherItem
import com.example.weatherappdemo.util.Constants.TEMP_STATUS
import com.example.weatherappdemo.util.Util
import com.example.weatherappdemo.util.WeatherDiffUtil


class ItemWeatherHourAdapter(var typeAdapterHour: TypeAdapterHour = TypeAdapterHour.MAIN) :
    RecyclerView.Adapter<ItemWeatherHourAdapter.ItemWeatherHourViewHolder>() {
    private var listData: MutableList<ListItem> = mutableListOf()

    fun setData(newData: List<ListItem>) {
        val diffUtil = DiffUtil.calculateDiff(WeatherDiffUtil(listData, newData))
        listData.clear()
        listData.addAll(newData)
        diffUtil.dispatchUpdatesTo(this)
    }

    public class ItemWeatherHourViewHolder(var itemWeatherHousBinding: ItemWeatherHousBinding) :
        RecyclerView.ViewHolder(itemWeatherHousBinding.root) {
        fun setData(main: Main, weatherItem: WeatherItem, time: Long) {
            itemWeatherHousBinding.tvItemHour.text = Util.convertTime(time, "HH aa")
            itemWeatherHousBinding.tvItemTemp.text =
                itemView.context.getString(R.string.temperature,
                    (main.temp.times(0.1)).toFloat().let { Util.formatTemperature(it) })
            Glide.with(itemWeatherHousBinding.imvItemStatus)
                .load("https://openweathermap.org/img/wn/${weatherItem.icon}@2x.png")
                .into(itemWeatherHousBinding.imvItemStatus)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemWeatherHourViewHolder {
        val itemWeatherHousBinding =
            ItemWeatherHousBinding.inflate(LayoutInflater.from(parent.context))
        return ItemWeatherHourViewHolder(itemWeatherHousBinding)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ItemWeatherHourViewHolder, position: Int) {
        val data = listData[position]
        if (typeAdapterHour == TypeAdapterHour.DETAIL) {
            holder.itemWeatherHousBinding.layoutItemRoot.setOnClickListener {
                listData[position].check = !listData[position].check
                val bgResource = if (data.check) {
                    if ((data.main?.temp
                            ?: 0.0) > TEMP_STATUS
                    ) R.drawable.bg_item_first_weather_hour_sun
                    else R.drawable.bg_item_first_weather_hour
                } else com.google.android.material.R.color.mtrl_btn_transparent_bg_color
                holder.itemWeatherHousBinding.layoutItemRoot.setBackgroundResource(bgResource)
                notifyItemChanged(position)
            }
        }
        data.main?.let { main ->
            data.weather?.get(0)?.let { weatherItem ->
                data.dt?.let { dt ->
                    holder.setData(
                        main, weatherItem, dt
                    )

                }
            }
        }

    }
}