package com.example.weatherappdemo.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.example.weatherappdemo.databinding.DialogExitAppBinding

class ExitDialog(context: Context, var onExit: () -> Unit) : Dialog(context) {

    private lateinit var binding: DialogExitAppBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogExitAppBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnExit.setOnClickListener {
            onExit()
        }

        binding.btnExitCancel.setOnClickListener {
            dismiss()
        }
    }

}
