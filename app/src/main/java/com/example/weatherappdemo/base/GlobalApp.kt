package com.example.weatherappdemo.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

class GlobalApp : Application() {
    companion object{
        @SuppressLint("StaticFieldLeak")
        lateinit var mContext : Context
    }
    override fun onCreate() {
        super.onCreate()
       mContext = applicationContext
    }
}