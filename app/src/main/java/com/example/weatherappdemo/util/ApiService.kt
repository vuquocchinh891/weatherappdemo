package com.example.weatherappdemo.util

import com.example.weatherappdemo.model.WeatherHourResponse
import com.example.weatherappdemo.model.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/data/2.5/weather")
    fun getDataLocation(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("lang") lang: String = "vi",
        @Query("units") units: String = " metric",
        @Query("appid") appId: String = Constants.API_KEY
    ): Call<WeatherResponse>

    @GET("/data/2.5/weather")
    fun getDataByNameCity(
        @Query("q") city: String,
        @Query("lang") lang: String = "vi",
        @Query("units") units: String = " metric",
        @Query("appid") appId: String = Constants.API_KEY
    ): Call<WeatherResponse>

    @GET("/data/2.5/forecast")
    fun getDataForecastLocation(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("lang") lang: String = "vi",
        @Query("units") units: String = " metric",
        @Query("appid") appId: String = Constants.API_KEY
    ): Call<WeatherHourResponse>

    @GET("/data/2.5/forecast")
    fun getDataForecastCity(
        @Query("q") city: String,
        @Query("lang") lang: String = "vi",
        @Query("units") units: String = " metric",
        @Query("appid") appId: String = Constants.API_KEY
    ): Call<WeatherHourResponse>

}