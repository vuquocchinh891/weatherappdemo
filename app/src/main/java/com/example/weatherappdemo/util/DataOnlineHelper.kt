package com.example.weatherappdemo.util

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataOnlineHelper {
    fun getInstance(): Retrofit {
        return Retrofit.Builder().baseUrl("https://api.openweathermap.org")
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
    var apiService : ApiService = getInstance().create(ApiService::class.java)
}