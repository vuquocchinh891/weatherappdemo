package com.example.weatherappdemo.util

import androidx.recyclerview.widget.DiffUtil
import com.example.weatherappdemo.model.ListItem

class WeatherDiffUtil(var oldListData: List<ListItem>, var newListData: List<ListItem>) :
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldListData.size
    }

    override fun getNewListSize(): Int {
        return newListData.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldListData[oldItemPosition].dt == newListData[newItemPosition].dt
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldListData[oldItemPosition] == newListData[newItemPosition]
    }
}