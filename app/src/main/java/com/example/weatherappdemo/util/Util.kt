package com.example.weatherappdemo.util

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.text.SimpleDateFormat
import java.util.Date


object Util {
    fun convertTime(dt: Long, format: String): String {
        val date = Date(dt * 1000L)
        val dateFormat = SimpleDateFormat(format)
        return dateFormat.format(date)
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun formatTemperature(temp: Float): String {
        return temp.toInt().toString()
    }

    fun msToKmh(mps: Double): Int {
        return (mps * 3.6).toInt()
    }
}