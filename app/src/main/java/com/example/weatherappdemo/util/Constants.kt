package com.example.weatherappdemo.util

object Constants {
    const val API_KEY = "7228850b3794cc91f88182d08536c5d4"
    const val KEY_DETAIL = "key_search"
    const val ERROR_API = 404
    const val KEY_LOCAL = "key_local"
    const val TAG_MAIN = "main_fragment"
    const val TAG_MAP = "map_fragment"
    const val TEMP_STATUS = 25
}