package com.example.weatherappdemo.fragment

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.example.weatherappdemo.R
import com.example.weatherappdemo.activity.DetailActivity
import com.example.weatherappdemo.activity.MainActivity
import com.example.weatherappdemo.adapter.ItemWeatherHourAdapter
import com.example.weatherappdemo.databinding.FragmentMainBinding
import com.example.weatherappdemo.model.LocalMap
import com.example.weatherappdemo.util.Constants
import com.example.weatherappdemo.util.Util
import com.example.weatherappdemo.viewmodel.FragmentMainViewModel
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class MainFragment : Fragment() {
    private lateinit var mainBinding: FragmentMainBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var itemWeatherHourAdapter = ItemWeatherHourAdapter()
    private lateinit var lineDataSet: LineDataSet
    private var mLocal = LocalMap()
    private var colorLineChart = intArrayOf(
        R.color.line_chart_1, R.color.line_chart_2, R.color.line_chart_3, R.color.line_chart_4
    )
    val mFragmentMainViewModel by lazy {
        ViewModelProvider(this)[FragmentMainViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        mainBinding = FragmentMainBinding.inflate(layoutInflater)
        activity?.let { activity ->
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
        }

        return mainBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initAdapter()
        checkPermission()
        observer()
        onClick()
    }

    private fun onClick() {
        mainBinding.tvMainNext7Day.setOnClickListener {
            activity?.let { act ->
                val intentSearch = Intent(act, DetailActivity::class.java)
                intentSearch.putExtra(
                    Constants.KEY_DETAIL, mFragmentMainViewModel.weatherHourResponse
                )
                startActivity(intentSearch)
            }
        }
    }


    private fun changeColorTheme(color: Int) {
        mainBinding.apply {
            context?.let { ctx ->
                tvMainHumidity.setTextColor(ctx.getColor(color))
                tvMainTemp.setTextColor(ctx.getColor(color))
                tvMainToday.setTextColor(ctx.getColor(color))
                tvMainPressure.setTextColor(ctx.getColor(color))
                tvMainLocateName.setTextColor(ctx.getColor(color))
                imvMainPressure.setColorFilter(ctx.getColor(color))
                imvMainWindy.setColorFilter(ctx.getColor(color))
                tvMainWindy.setTextColor(ctx.getColor(color))
                tvMainTitleChart.setTextColor(ctx.getColor(color))
                tvMainToday.setTextColor(ctx.getColor(color))
                when (color) {
                    R.color.temp_up_25 -> {
                        imvMainWeatherType.setImageResource(R.drawable.bg_sun_day)
                    }

                    else -> {
                        imvMainWeatherType.setImageResource(R.drawable.bg_cloud_day)
                    }
                }

            }
        }
    }


    private fun initAdapter() {
        mainBinding.rcvMainWeatherTodayHour.adapter = itemWeatherHourAdapter
    }

    private fun observer() {
        mFragmentMainViewModel.dataMain.observe(viewLifecycleOwner) { weather ->
            if (weather.cod == MainActivity.CODE_SUSS) {
                mainBinding.apply {
                    tvMainTemp.text = getString(R.string.temperature,
                        (weather.main?.temp?.times(0.1))?.toFloat()?.let { temp ->

                            changeColorTheme(if (temp > Constants.TEMP_STATUS) R.color.temp_up_25 else R.color.temp_under_25)
                            Util.formatTemperature(temp)
                        })
                    tvMainHumidity.text = "${weather.main?.humidity}%"
                    tvMainPressure.text =
                        getString(R.string.pressure, weather.main?.pressure.toString())
                    tvMainWindy.text = getString(
                        R.string.speed_wind, Util.msToKmh(weather.wind?.speed ?: 0.0).toString()
                    )
                    tvMainLocateName.text = weather.name
                    tvMainTimeUpdate.text = Util.convertTime(weather.dt ?: 0, "HH:mm")
                    tvMainStatusWeather.text = weather.weather?.get(0)?.description ?: ""
                }
            }
        }
        mFragmentMainViewModel.dataForecast.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                itemWeatherHourAdapter.setData(it)
                val listCopy = it.subList(0, 4)
                val listDataChart = mutableListOf<Entry>()
                for (i in listCopy.indices) {
                    val entry = Entry(
                        i.toFloat(), ((listCopy[i].main?.temp as Double) * 0.1).toFloat()
                    )
                    listDataChart.add(entry)
                }
                upDateData(listDataChart)
            }
        }
    }


    private fun initViews() {
        upDateData(data())
    }

    private fun upDateData(dataTemp: MutableList<Entry>) {
        lineDataSet = LineDataSet(dataTemp, "Data")

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(lineDataSet)

        lineDataSet.apply {
            lineWidth = 2f
            setDrawCircleHole(true)
            lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
            setColors(colorLineChart, activity)
        }
        val lineData = LineData(dataSets)
        val listXValue = listOf(
            getString(R.string.txt_morning),
            getString(R.string.txt_afternoon),
            getString(R.string.txt_evening),
            getString(R.string.txt_night),
        )

        val xAxis = mainBinding.chartMainTemperature.xAxis
        xAxis.apply {
            disableAxisLineDashedLine()
            position = XAxis.XAxisPosition.BOTTOM
            valueFormatter =
                (com.github.mikephil.charting.formatter.IndexAxisValueFormatter(listXValue))
        }

        mainBinding.chartMainTemperature.apply {
            data = lineData
            invalidate()
            legend.isEnabled = false
            description.isEnabled = false
            axisLeft.isEnabled = false
            axisRight.isEnabled = false
        }
    }

    private fun data(): ArrayList<Entry> {
        return arrayListOf()
    }


    private fun checkPermission() {
        activity?.let { context ->
            if (ActivityCompat.checkSelfPermission(
                    context, android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    context,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    MainActivity.REQUEST_CODE_LOCATION
                )
            } else {
                try {
                    val locationManager =
                        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        getLastLocation()
                    } else {
                        //show dialog
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                } catch (e: SecurityException) {

                }

            }
        }
    }

    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location ->
            mLocal = LocalMap(location.latitude, location.longitude)
            mFragmentMainViewModel.getDataByLatLong(location.latitude, location.longitude)
//            mLocal = Local(location.latitude, location.longitude)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MainActivity.REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            } else {
                checkPermission()
            }
        }
    }

}