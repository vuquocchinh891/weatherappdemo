package com.example.weatherappdemo.fragment

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.weatherappdemo.R
import com.example.weatherappdemo.databinding.FragmentMapBinding
import com.example.weatherappdemo.model.LocalMap
import com.example.weatherappdemo.model.WeatherResponse
import com.example.weatherappdemo.util.Constants
import com.example.weatherappdemo.util.Util
import com.example.weatherappdemo.viewmodel.FragmentMapViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mBinding: FragmentMapBinding

    private lateinit var mMap: GoogleMap
    private var mLocation = LatLng(20.9905381, 105.7987653)
    private lateinit var mapFragment: SupportMapFragment
    private var weatherResponse = WeatherResponse()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    val fragmentMapViewModel by lazy {
        ViewModelProvider(this)[FragmentMapViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentMapBinding.inflate(layoutInflater)

        activity?.let { activity ->
            mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
        }
        observer()
        onClick()
        getLocation()
        hashData()
        return mBinding.root
    }

    private fun hashData() {
        val dataCity = listOf(
            LocalMap(10.924067, 106.713028),
            LocalMap(17.974855, 102.630867),
            LocalMap(21.099649, 102.251564)
        )

        dataCity.forEach { localMap ->
            fragmentMapViewModel.getDataByLatLong(localMap.lat, localMap.lon)
        }
    }


    private fun getLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location: Location ->
            mLocation = LatLng(location.latitude, location.longitude)
            fragmentMapViewModel.getDataByLatLong(location.latitude, location.longitude)
        }
    }

    private fun onClick() {
        mBinding.edtMapSearch.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if ((event.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    fragmentMapViewModel.getData(mBinding.edtMapSearch.text.toString())
                    activity?.let { Util.hideKeyboard(it) }
                    return true
                }
                return false
            }

        })
        mBinding.imvMapLocation.setOnClickListener {
            getLocation()
        }
    }


    private fun observer() {
        fragmentMapViewModel.dataMain.observe(viewLifecycleOwner) {
            activity?.let { activity ->
                if (it.cod != Constants.ERROR_API) {
                    weatherResponse = it
                    mapFragment.getMapAsync(this)
                    mLocation = LatLng(it.coord?.lat ?: 0.0, it.coord?.lon ?: 0.0)
                } else {
                    Toast.makeText(
                        activity, getString(R.string.txt_warning_city_name), Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        val makerView: View =
            (activity?.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                R.layout.maker_view_custom, null
            )
        val tvNameCity = makerView.findViewById<TextView>(R.id.tv_maker_name_city)
        val cvContent = makerView.findViewById<ConstraintLayout>(R.id.cv_maker_content)
        val imvStatusWeather = makerView.findViewById<ImageView>(R.id.image_maker_status)
        val tvTemp = makerView.findViewById<TextView>(R.id.tv_maker_temp)
        tvNameCity.text = weatherResponse.name
        tvTemp.text = getString(R.string.temperature,
            weatherResponse.main?.temp?.times(0.1)?.let { Util.formatTemperature(it.toFloat()) })
        Glide.with(imvStatusWeather).asBitmap()
            .load(getString(R.string.icon_url, weatherResponse.weather?.get(0)?.icon))
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap, transition: Transition<in Bitmap>?
                ) {
                    imvStatusWeather.setImageBitmap(resource)
                    makerView.measure(
                        View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED
                    )
                    makerView.layout(
                        0, 0, makerView.measuredWidth, makerView.measuredHeight
                    )
                    makerView.buildDrawingCache()
                    val bmContent = Bitmap.createScaledBitmap(
                        viewToBitmap(cvContent), cvContent.width, cvContent.height, false
                    )
                    val smallMakerIcon = BitmapDescriptorFactory.fromBitmap(bmContent)

                    mMap.addMarker(
                        MarkerOptions().position(mLocation).icon(smallMakerIcon)
                    )
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(mLocation))
                }

                override fun onLoadCleared(placeholder: Drawable?) {

                }
            })
    }

    private fun viewToBitmap(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
}