package com.example.weatherappdemo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class WeatherResponse(

    @field:SerializedName("visibility") val visibility: Int? = null,

    @field:SerializedName("timezone") val timezone: Int? = null,

    @field:SerializedName("main") val main: Main? = null,

    @field:SerializedName("clouds") val clouds: Clouds? = null,

    @field:SerializedName("sys") val sys: Sys? = null,

    @field:SerializedName("dt") val dt: Long? = null,

    @field:SerializedName("coord") val coord: Coord? = null,

    @field:SerializedName("weather") val weather: List<WeatherItem?>? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("cod") val cod: Int? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("base") val base: String? = null,

    @field:SerializedName("wind") val wind: Wind? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readParcelable(Main::class.java.classLoader),
        parcel.readParcelable(Clouds::class.java.classLoader),
        parcel.readParcelable(Sys::class.java.classLoader),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readParcelable(Coord::class.java.classLoader),
        parcel.createTypedArrayList(WeatherItem),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readParcelable(Wind::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(visibility)
        parcel.writeValue(timezone)
        parcel.writeParcelable(main, flags)
        parcel.writeParcelable(clouds, flags)
        parcel.writeParcelable(sys, flags)
        parcel.writeValue(dt)
        parcel.writeParcelable(coord, flags)
        parcel.writeTypedList(weather)
        parcel.writeString(name)
        parcel.writeValue(cod)
        parcel.writeValue(id)
        parcel.writeString(base)
        parcel.writeParcelable(wind, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherResponse> {
        override fun createFromParcel(parcel: Parcel): WeatherResponse {
            return WeatherResponse(parcel)
        }

        override fun newArray(size: Int): Array<WeatherResponse?> {
            return arrayOfNulls(size)
        }
    }

}

data class Wind(

    @field:SerializedName("deg") val deg: Int? = null,

    @field:SerializedName("speed") val speed: Double? = null,

    @field:SerializedName("gust") val gust: Double? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(deg)
        parcel.writeValue(speed)
        parcel.writeValue(gust)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Wind> {
        override fun createFromParcel(parcel: Parcel): Wind {
            return Wind(parcel)
        }

        override fun newArray(size: Int): Array<Wind?> {
            return arrayOfNulls(size)
        }
    }

}

data class WeatherItem(

    @field:SerializedName("icon") val icon: String? = null,

    @field:SerializedName("description") val description: String? = null,

    @field:SerializedName("main") val main: String? = null,

    @field:SerializedName("id") val id: Int? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(icon)
        parcel.writeString(description)
        parcel.writeString(main)
        parcel.writeValue(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherItem> {
        override fun createFromParcel(parcel: Parcel): WeatherItem {
            return WeatherItem(parcel)
        }

        override fun newArray(size: Int): Array<WeatherItem?> {
            return arrayOfNulls(size)
        }
    }
}

data class Main(

    @field:SerializedName("temp") val temp: Double = 0.0,

    @field:SerializedName("temp_min") val tempMin: Double = 0.0,

    @field:SerializedName("grnd_level") val grndLevel: Int = 0,

    @field:SerializedName("humidity") val humidity: Int = 0,

    @field:SerializedName("pressure") val pressure: Int = 0,

    @field:SerializedName("sea_level") val seaLevel: Int = 0,

    @field:SerializedName("feels_like") val feelsLike: Double = 0.0,

    @field:SerializedName("temp_max") val tempMax: Double = 0.0
) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(temp)
        parcel.writeDouble(tempMin)
        parcel.writeInt(grndLevel)
        parcel.writeInt(humidity)
        parcel.writeInt(pressure)
        parcel.writeInt(seaLevel)
        parcel.writeDouble(feelsLike)
        parcel.writeDouble(tempMax)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Main> {
        override fun createFromParcel(parcel: Parcel): Main {
            return Main(parcel)
        }

        override fun newArray(size: Int): Array<Main?> {
            return arrayOfNulls(size)
        }
    }

}

data class Clouds(

    @field:SerializedName("all") val all: Int? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(all)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Clouds> {
        override fun createFromParcel(parcel: Parcel): Clouds {
            return Clouds(parcel)
        }

        override fun newArray(size: Int): Array<Clouds?> {
            return arrayOfNulls(size)
        }
    }

}

data class Coord(

    @field:SerializedName("lon") val lon: Double? = null,

    @field:SerializedName("lat") val lat: Double? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Double::class.java.classLoader) as? Double
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(lon)
        parcel.writeValue(lat)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Coord> {
        override fun createFromParcel(parcel: Parcel): Coord {
            return Coord(parcel)
        }

        override fun newArray(size: Int): Array<Coord?> {
            return arrayOfNulls(size)
        }
    }

}


data class Sys(

    @field:SerializedName("country") val country: String? = null,

    @field:SerializedName("sunrise") val sunrise: Int? = null,

    @field:SerializedName("sunset") val sunset: Int? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("type") val type: Int? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeValue(sunrise)
        parcel.writeValue(sunset)
        parcel.writeValue(id)
        parcel.writeValue(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Sys> {
        override fun createFromParcel(parcel: Parcel): Sys {
            return Sys(parcel)
        }

        override fun newArray(size: Int): Array<Sys?> {
            return arrayOfNulls(size)
        }
    }

}
