package com.example.weatherappdemo.model

import android.os.Parcel
import android.os.Parcelable

data class LocalMap(var lat: Double = 0.0, var lon: Double = 0.0) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(), parcel.readDouble()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(lat)
        parcel.writeDouble(lon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocalMap> {
        override fun createFromParcel(parcel: Parcel): LocalMap {
            return LocalMap(parcel)
        }

        override fun newArray(size: Int): Array<LocalMap?> {
            return arrayOfNulls(size)
        }
    }

}