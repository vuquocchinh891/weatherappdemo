package com.example.weatherappdemo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class WeatherHourResponse(

    @field:SerializedName("city") val city: City? = null,

    @field:SerializedName("cnt") val cnt: Int? = null,

    @field:SerializedName("cod") val cod: String? = null,

    @field:SerializedName("message") val message: Int? = null,

    @field:SerializedName("list") val list: List<ListItem>? = listOf()
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(City::class.java.classLoader),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.createTypedArrayList(ListItem)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(city, flags)
        parcel.writeValue(cnt)
        parcel.writeString(cod)
        parcel.writeValue(message)
        parcel.writeTypedList(list)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherHourResponse> {
        override fun createFromParcel(parcel: Parcel): WeatherHourResponse {
            return WeatherHourResponse(parcel)
        }

        override fun newArray(size: Int): Array<WeatherHourResponse?> {
            return arrayOfNulls(size)
        }
    }

}

data class Rain(

    @field:SerializedName("3h") val jsonMember3h: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(jsonMember3h)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rain> {
        override fun createFromParcel(parcel: Parcel): Rain {
            return Rain(parcel)
        }

        override fun newArray(size: Int): Array<Rain?> {
            return arrayOfNulls(size)
        }
    }
}


data class ListItem(

    @field:SerializedName("dt") val dt: Long? = null,

    @field:SerializedName("pop") val pop: String? = null,

    @field:SerializedName("visibility") val visibility: Int? = null,

    @field:SerializedName("dt_txt") val dtTxt: String? = null,

    @field:SerializedName("weather") val weather: List<WeatherItem?>? = null,

    @field:SerializedName("main") val main: Main? = Main(),

    @field:SerializedName("clouds") val clouds: Clouds? = null,

    @field:SerializedName("sys") val sys: Sys? = null,

    @field:SerializedName("wind") val wind: Wind? = null,

    @field:SerializedName("rain") val rain: Rain? = null,
    @field:SerializedName("check") var check: Boolean = false
) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.createTypedArrayList(WeatherItem),
        parcel.readParcelable(Main::class.java.classLoader),
        parcel.readParcelable(Clouds::class.java.classLoader),
        parcel.readParcelable(Sys::class.java.classLoader),
        parcel.readParcelable(Wind::class.java.classLoader),
        parcel.readParcelable(Rain::class.java.classLoader),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(dt)
        parcel.writeString(pop)
        parcel.writeValue(visibility)
        parcel.writeString(dtTxt)
        parcel.writeTypedList(weather)
        parcel.writeParcelable(main, flags)
        parcel.writeParcelable(clouds, flags)
        parcel.writeParcelable(sys, flags)
        parcel.writeParcelable(wind, flags)
        parcel.writeParcelable(rain, flags)
        parcel.writeByte(if (check) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ListItem> {
        override fun createFromParcel(parcel: Parcel): ListItem {
            return ListItem(parcel)
        }

        override fun newArray(size: Int): Array<ListItem?> {
            return arrayOfNulls(size)
        }
    }

}

data class City(

    @field:SerializedName("country") val country: String? = null,

    @field:SerializedName("coord") val coord: Coord? = null,

    @field:SerializedName("sunrise") val sunrise: Int? = null,

    @field:SerializedName("timezone") val timezone: Int? = null,

    @field:SerializedName("sunset") val sunset: Int? = null,

    @field:SerializedName("name") val name: String? = null,

    @field:SerializedName("id") val id: Int? = null,

    @field:SerializedName("population") val population: Int? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(Coord::class.java.classLoader),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeParcelable(coord, flags)
        parcel.writeValue(sunrise)
        parcel.writeValue(timezone)
        parcel.writeValue(sunset)
        parcel.writeString(name)
        parcel.writeValue(id)
        parcel.writeValue(population)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<City> {
        override fun createFromParcel(parcel: Parcel): City {
            return City(parcel)
        }

        override fun newArray(size: Int): Array<City?> {
            return arrayOfNulls(size)
        }
    }

}



