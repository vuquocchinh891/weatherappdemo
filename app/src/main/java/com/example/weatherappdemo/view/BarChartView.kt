package com.example.weatherappdemo.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import java.util.Collections


class BarChartView(context: Context?, attrs: AttributeSet?) :
    View(context, attrs) {
    private val mPaint: Paint = Paint()
    private var mData: List<Int>

    init {
        mPaint.color = Color.BLUE
        mData = ArrayList()
    }

    fun setData(data: List<Int>) {
        mData = data
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val width = width
        val height = height
        val barWidth = width / mData.size
        val maxValue = Collections.max(mData)
        for (i in mData.indices) {
            val barHeight = mData[i].toFloat() / maxValue.toFloat() * height.toFloat()
            val left = (i * barWidth).toFloat()
            val top = height - barHeight
            val right = left + barWidth
            val bottom = height.toFloat()
            canvas.drawRect(left, top, right, bottom, mPaint)
        }
    }
}